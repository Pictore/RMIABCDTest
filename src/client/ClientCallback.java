package client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Scanner;

import interfaces.*;

public class ClientCallback
        extends UnicastRemoteObject
        implements ICallback {
    public ClientCallback() throws RemoteException {
        super();
    }

    // Receive a structure of questions from the server, run the test, return the score.
    public int receiveQuestions(List que) throws RemoteException{
        int score = ChatClient.runTest(que);
        return score;
    }

    public void exitClient() throws RemoteException{
        ChatClient.exit();
    }
}

