package client;

import interfaces.ICallback;
import interfaces.IChat;
import javafx.util.Pair;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.Scanner;

public class ChatClient {
    IChat remoteObject;
    ICallback callback;
    Scanner userInput = new Scanner(System.in);
    String username;
    public static void main(String[] args) {
        if(args.length < 1) {
            System.out.println("Usage: ChatClient <server host name>");
            System.exit(-1);
        }
        new ChatClient(args[0]);
    }

    public ChatClient(String hostname) {
        System.out.println("Enter your name: ");
        username = userInput.nextLine();
        Registry reg;
        try {
            reg = LocateRegistry.getRegistry(hostname);
            // Finding remote object by its name.
            remoteObject = (IChat) reg.lookup("ChatServer");
            callback = new ClientCallback();
            // Remote object's methods.
            remoteObject.register(username, callback);
            remoteObject.sendQuestions(username, callback);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }


    static int runTest(List que) {
        Scanner userInput = new Scanner(System.in);
        String line;
        boolean condition;
        int score = 0;

        for(int i = 0; i < que.size(); i++) {
            condition = true;
            // One question and a list of answers per iteration.
            Pair<String, List<String>> tmp = (Pair) que.get(i);
            List<String> ans = tmp.getValue();
            System.out.println(tmp.getKey());
            for(int j = 0; j < 4; j++) {
                System.out.println(ans.get(j));
            }
            // A loop for answer in format of A/B/C/D
            while(condition) {
                line = userInput.nextLine();
                if (!line.matches("[ABCD]")){
                    System.out.println("Please provide A/B/C/D answer");
                    continue;
                }
                else {
                    // Check if the answer is correct
                    if(line.equals(ans.get(4))) {
                        System.out.println("Correct Answer");
                        score++;
                    }
                    else {
                        System.out.println("Wrong Answer");
                    }
                    condition = false;
                }
            }
        }
        System.out.println("Your final score: " + score + "/" + que.size());
        // Return score to Callback function
        return score;
    }

    // Function for exiting the client run by the server.
    static void exit() {
        System.exit(0);
    }

}
