package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ICallback extends Remote{
    int receiveQuestions(List que) throws RemoteException;
    void exitClient() throws RemoteException;
}
