package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface IChat extends Remote{
    boolean register(String nick, ICallback n) throws RemoteException;
    boolean sendQuestions(String nick, ICallback n) throws RemoteException;
    void loadQuiz() throws RemoteException;
}
