package server;

import java.io.*;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import interfaces.*;
import javafx.util.Pair;

public class ChatServant
extends UnicastRemoteObject
implements IChat {
    private Map<String, ICallback> users = new ConcurrentHashMap<>();
    private Map<String, Integer> scores = new ConcurrentHashMap<>();
    private List<Pair<String, List<String>>> questions = new ArrayList<>();
    public ChatServant() throws RemoteException {
    }

    // Function registering the user.
    public boolean register(String nick, ICallback n) throws RemoteException{
        Thread T1 = new Register(nick, n, users);
        T1.start();
        return true;
    }

    // Function sending the questions to the client.
    public boolean sendQuestions(String nick, ICallback n) throws RemoteException{
        Thread T1 = new SendQuestions(nick, n, questions, scores);
        T1.start();
        return true;
    }

    // Function loading the quiz from "test.txt" file in a specific format.
    public void loadQuiz() throws RemoteException{
        String path = System.getProperty("user.dir");
        String fileName = path + "\\test.txt";
        File file = new File(fileName);

        try {
            Scanner sc = new Scanner(file);
            while(sc.hasNextLine()){
                String q = sc.nextLine();
                List<String> ans = new ArrayList<>();
                ans.add(sc.nextLine());
                ans.add(sc.nextLine());
                ans.add(sc.nextLine());
                ans.add(sc.nextLine());
                ans.add(sc.nextLine());
                questions.add(new Pair<>(q, ans));
            }
            sc.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");
        }

    }
}
