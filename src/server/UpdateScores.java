package server;

import interfaces.ICallback;
import java.rmi.RemoteException;
import java.util.Map;

public class UpdateScores extends Thread {
    private Thread t;
    private String threadName;
    private Integer score;
    private Map<String, Integer> scores;
    private ICallback call;

    UpdateScores( String name, Integer n, Map u, ICallback b) {
        threadName = name;
        score = n;
        scores = u;
        call = b;
        System.out.println("Scoring " +  threadName );
    }

    public void run() {
        System.out.println("Scoring " +  threadName );
        try {
            // Update the scoreboard.
            if (!scores.containsKey(threadName)) {
                scores.put(threadName, score);
            }
            // Print out the scoreboard to ensure proper execution.
            System.out.print(scores);
            // Exit the client as it is no longer needed, results in RemoteException.
            call.exitClient();
            Thread.sleep(800);
        } catch (InterruptedException e) {
            System.out.println("Thread " +  threadName + " interrupted.");
        } catch (RemoteException e) {
            System.out.println("Closing the client");
        }
        System.out.println("Thread scoring " +  threadName + " exiting.");
    }

    public void start () {
        System.out.println("Starting scoring " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}
