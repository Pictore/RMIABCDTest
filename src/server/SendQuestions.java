package server;

import interfaces.ICallback;
import javafx.util.Pair;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

public class SendQuestions extends Thread {
    private Thread t;
    private String threadName;
    private ICallback call;
    private List<Pair<String, List<String>>> questions;
    private Map<String, Integer> scores;

    SendQuestions( String name, ICallback n, List u, Map sc) {
        threadName = name;
        call = n;
        questions = u;
        scores = sc;
        System.out.println(threadName);
    }

    public void run() {
        System.out.println("Sending questions " +  threadName );
        try {
            int score = call.receiveQuestions(questions);
            // After the score is received from the callback, the scoreboard is updated.
            Thread T2 = new UpdateScores(threadName, score, scores, call);
            T2.start();
            Thread.sleep(800);
        } catch (InterruptedException e) {
            System.out.println("Thread " +  threadName + " interrupted.");
        } catch (RemoteException e) {
            System.out.println("Remote exception");
        }
        System.out.println("Sending questions " +  threadName + " exiting.");
    }

    public void start () {
        System.out.println("Starting sending questions " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }


}
