package server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ChatServer {
    Registry reg;
    ChatServant servant;

    public static void main(String[] args) {
        try {
            new ChatServer();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    protected ChatServer() throws RemoteException {
        try {
            reg = LocateRegistry.createRegistry(1099);
            servant = new ChatServant();
            reg.rebind("ChatServer", servant);
            servant.loadQuiz(); // The questions are loaded as the server starts.
            System.out.println("ChatServer READY");

        } catch(RemoteException e) {
            e.printStackTrace();
            throw e;
        }
    }
}

