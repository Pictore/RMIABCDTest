package server;

import interfaces.ICallback;
import java.util.Map;


public class Register extends Thread {
    private Thread t;
    private String threadName;
    private ICallback call;
    private Map<String, ICallback> userlist;

    Register(String name, ICallback n, Map u) {
        threadName = name;
        call = n;
        userlist = u;
        System.out.println("Registering " +  threadName );
    }

    public void run() {
        System.out.println("Registering " +  threadName );
        try {
             if (!userlist.containsKey(threadName)) {
                    userlist.put(threadName, call);
                }
                // Let the thread sleep for a while.
                Thread.sleep(800);
        } catch (InterruptedException e) {
            System.out.println("Thread " +  threadName + " interrupted.");
        }
        System.out.println("Registering " +  threadName + " exiting.");
    }

    public void start () {
        System.out.println("Starting registering " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}
